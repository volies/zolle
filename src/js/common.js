// Document READY
$(document).ready(function(){
	initSlick();
	function initSlick(){
		$('.on-carousel').slick({
			infinite: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			dots: false
		});
	}

	(function(){
		var carousel = $('.on-carousel'),
			slides = carousel.find('.slick-slide:not(.slick-cloned)'),
			lightbox = slides.find('.lightbox-slide');

		lightbox.attr('data-lightbox', 'gall');
	}());
	
	// Json load
	(function(){

		var worksFile = $.getJSON( "works.json", function(data) {
			var worksHtml = "",
				indexDelay = 0;

			data.works.forEach(function( val, ind ){
				if ( ind == 3 ) {
					$('#flats-main').html(worksHtml);
					worksHtml = "";
					indexDelay = 0;
				}
				
				var animDelay = 0.5 + (0.3 * indexDelay++);

				worksHtml += "<div class='more-flats__flat wow fadeInRight' data-wow-delay='"+animDelay+"s'><div class='flat' style='background-image: url("+val.images[0]+");' data-id='"+val.id+"'><p class='flat__name'>"+val.name+"</p></div></div>";
			});
			$('#flats-other').html(worksHtml);
		});

	}());

	(function(){
		var carousel = $('.on-carousel'),
			animateLine = false;

		$('.more-flats').on('click', '.flat', function(){
			
			if ( animateLine == true ) return false;
			
			animateLine = true;

			var thisId = $(this).data('id'),
				thisWork = null;

			// Поиск опред. работы
			var worksFile = $.getJSON( "works.json", function(data) {
				for (var i = 0; i < data.works.length; i++) {
					if ( data.works[i].id == thisId ) {
						thisWork = data.works[i];
						break;
					}
				}
			})
			.done(insertWork);

			function insertWork() {
				var htmlSlides = "";
				thisWork.images.forEach(function(val, ind){
					htmlSlides += '<div class="carousel__slide" style="background-image: url('+val+');"><a href="'+val+'" class="lightbox-slide" data-lightbox="gall"><img src="'+val+'" alt=""></a></div>';
				});
				

				var tl = new TimelineMax();    
				tl.to($('.carousel-name'), .5, {opacity: 0})
				.to($('.carousel'), .5, {opacity: 0})
				.addCallback(function(){
					$('.carousel-name').text(thisWork.name);
					$('.on-carousel').slick('unslick');
					$('.on-carousel').html(htmlSlides);
					initSlick();
				})
				.to($('.carousel'), .5, {opacity: 1, ease:Linear.easeIn})
				.to($('.carousel-name'), .5, {opacity: 1})
				.to($('.blurry-bg'), 1, {opacity: 0}, "-=2")
				.addCallback(function(){
					$('.blurry-bg').css({
						'background-image': "url("+thisWork.images[0]+")"
					});
				})
				.to($('.blurry-bg'), 1, {opacity: .3})
				.addCallback(function(){
					animateLine = false;
				});

			}
			
		});
	}());

	$('.on-load-more').click(function(e){
		e.preventDefault();
		$('#flats-other').css('display','flex');
	});

});

// WINDOW LOAD
$(window).on('load',function(){
	$('.content').removeClass('content-hide');
	new WOW().init();
});